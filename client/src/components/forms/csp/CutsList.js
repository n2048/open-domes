import React from 'react';

class CutsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { items: [] };
    }
    render() {
        if(this.props.items.length === 0 ) return <div></div>

        return <table className='CutsList'>
            <tbody>

                {this.props.items.map(item => {
                    return (
                    
                        <tr key={item.id} id={item.id}>
                            <td>{item.size}</td>
                            <td>{item.count}</td>  
                            <td>
                                <button onClick={()=> this.props.deleteCallback(item.id)}> Delete </button>
                            </td>
                        </tr>
                    )
                })}            
            </tbody>
        </table>
    }
}

export default CutsList;
