import React from 'react';

class StockList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { items: [] };
    }

    render() {
        if(this.props.items.length === 0 ) return <div></div>

        return <table className='StockList'>
            <tbody>
                {this.props.items.map(item => {
                    return (
                        <tr key={item.id} id={item.id}>
                            <td>{item.size}</td>
                            <td>{item.cost} EUR</td>  
                            <td>
                                <button onClick={()=> this.props.deleteCallback(item.id)}> Delete </button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    }
}

export default StockList;
  