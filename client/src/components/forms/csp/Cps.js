import './Cps.css';
import React from 'react';
import StockList from './StockList';
import CutsList from './CutsList';
import { v4 } from 'uuid';
import env from "react-dotenv";
import Papa from "papaparse";
import { CSVLink } from "react-csv";

const allowedExtensions = ["csv"];
class Cps extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      error: null,
      loading: false,
      stock: [],
      cuts: [],
      bladeSize: 0,
      newCut: {
        size: 0,
        count: 0
      },
      newStock: {
        size: 0,
        cost: 0
      },
      apiResult: {
        summary: {},
        order: [],
        cuttingPlan: [],
      }
    };
    this.addNewCut = this.addNewCut.bind(this);
    this.addNewStock = this.addNewStock.bind(this);
    this.handleBladeSizeChange = this.handleBladeSizeChange.bind(this);

    this.getCuttingPlan = this.getCuttingPlan.bind(this);

    this.handleNewStockSizeChange = this.handleNewStockSizeChange.bind(this);
    this.handleNewStockCostChange = this.handleNewStockCostChange.bind(this);
    this.handleNewCutSizeChange = this.handleNewCutSizeChange.bind(this);
    this.handleNewCutCountChange = this.handleNewCutCountChange.bind(this);

    this.deleteStock = this.deleteStock.bind(this);
    this.deleteCut = this.deleteCut.bind(this);

    this.clearPlan = this.clearPlan.bind(this);

    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleParse = this.handleParse.bind(this);
      
  }

  formatFloat(num) {
    return Math.round((parseFloat(num) + Number.EPSILON) * 100) / 100;
  }

  clearPlan () {
    this.setState({
      apiResult: {
          summary: {},
          order: [],
          cuttingPlan: [], 
      }
    });
  }
  getCuttingPlan(){

    const bladeSize = this.state.bladeSize;    
    const stock = this.state.stock.map(({ size, cost }) => ({ size, cost}));
    const cuts = this.state.cuts.map(({ size, count }) => ({ size, count}));
    

    const data = {
      bladeSize,
      stock, 
      cuts
    };

    if(!bladeSize || parseFloat(bladeSize) < 0){
      return this.setState({error: "Blade Size must be a positive value"});      
    }

        
    if(!stock || stock.length < 1){
      return this.setState({error: "Stock must contain items"});      
    }

            
    if(!cuts || cuts.length < 1){
      return this.setState({error: "List of required cuts must contain items"});      
    }

    this.setState({
      apiResult: {
          summary: {},
          order: [],
          cuttingPlan: [], 
      },
      error: null,
      loading: true
    });

    fetch(`${env.API_URL}/api/v1/csp`, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            apiResult: result ,
            loading: false,
            error: null
          });
        },
        (error) => {
          this.setState({loading: false, error: "error"});
          console.error(error);
        }
      ).catch(error => {
        this.setState({loading: false, error: "error"});
        console.error(error);
    })
  }

  deleteStock(id) {
    this.setState({ stock: this.state.stock.filter(s => s.id !== id) });
  }
  deleteCut(id) {
    this.setState({ cuts: this.state.cuts.filter(s => s.id !== id) });
  }
  handleNewStockSizeChange(e) {
    this.setState({ newStock: { ...this.state.newStock, size: this.formatFloat(e.target.value) } });
  }
  handleNewStockCostChange(e) {
    this.setState({ newStock: { ...this.state.newStock, cost: this.formatFloat(e.target.value) } });
  }
  handleNewCutSizeChange(e) {
    this.setState({ newCut: { ...this.state.newCut, size: this.formatFloat(e.target.value) } });
  }
  handleNewCutCountChange(e) {
    this.setState({ newCut: { ...this.state.newCut, count: parseInt(e.target.value) } });
  }

  addNewCut(e) {
    e.preventDefault();
    if (this.state.newCut.size > 0 && this.state.newCut.count > 0) {
      this.setState({ cuts: [...this.state.cuts, { ...this.state.newCut, id: v4() }] });
    }
  }

  addNewStock(e) {
    e.preventDefault();
    if (this.state.newStock.size > 0 && this.state.newStock.cost > 0) {
      this.setState({ stock: [...this.state.stock, { ...this.state.newStock, id: v4() }] });
    }
  }
  handleBladeSizeChange(e) {
    this.setState({ bladeSize: this.formatFloat(e.target.value) });
  }

  handleFileChange(e) {
        this.setState({error: ''});
         
        // Check if user has entered the file
        if (e.target.files.length) {
            const inputFile = e.target.files[0];
             
            // Check the file extensions, if it not
            // included in the allowed extensions
            // we show the error
            const fileExtension = inputFile?.type.split("/")[1];
            if (!allowedExtensions.includes(fileExtension)) {
                this.setState({error: "Please input a csv file"});
                return;
            }
 
            // If input type is correct set the state
            // this.setState({file: inputFile});
            this.handleParse(inputFile);
        }
    };
    handleParse(file) {       
        // Initialize a reader which allows user
        // to read any file or blob.
        const reader = new FileReader();
         
        // Event listener on reader when the file
        // loads, we parse it and set the data.
        reader.onload = async ({ target }) => {
            const csv = Papa.parse(target.result, { header: true });
            const parsedData = csv?.data;
            const columns = Object.keys(parsedData[0]); 
            if(columns.indexOf('cost')!==-1){
              console.log('loading stock');
              const stock =  parsedData.map(l=>{return {
                cost: this.formatFloat(l.cost),
                size: this.formatFloat(l.size),
                id: v4()
              }});
              this.setState({stock});
              
            }
            else if (columns.indexOf('count')!==-1){
              console.log('loading cuts');
              const cuts =  parsedData.map(l=>{return {
                count: parseInt(l.count),
                size: this.formatFloat(l.size),
                id: v4()
              }});
              this.setState({cuts});
            }

            this.setState({data: columns});
        };
        reader.readAsText(file);
    };





  render() {
    return (
      <div className="Cps">
        <p>Cutting Stock Solver</p>
        <hr />
        {/* <label htmlFor="bladeSizeInput">Blade Size [mm]</label> */}
        <br />
        <input
          id="bladeSizeInput"
          type="number"
          onChange={this.handleBladeSizeChange}
          // defaultValue={this.state.bladeSize}
          placeholder="blade size"
        />
        <br />
        <hr />
        <input
          id="newStockSize"
          type="number"
          onChange={this.handleNewStockSizeChange}
          // defaultValue={this.state.newStock.size}
          placeholder="Size"
        />
        <input
          id="newStockCost"
          type="number"
          onChange={this.handleNewStockCostChange}
          // defaultValue={this.state.newStock.cost}
          placeholder="Cost"
        />
        <button onClick={this.addNewStock}>
          Add stock material #{this.state.stock.length + 1}
        </button>
        <StockList items={this.state.stock} deleteCallback={this.deleteStock} />
        <br />
        <hr />
        <input
          id="newCutSize"
          type="number"
          onChange={this.handleNewCutSizeChange}
          // defaultValue={this.state.newCut.size}
          placeholder="Size"
        />
        <input
          id="newCutCount"
          type="number"
          onChange={this.handleNewCutCountChange}
          // defaultValue={this.state.newCut.count}
          placeholder="Count"
        />
        <button onClick={this.addNewCut}>
          Add cut #{this.state.cuts.length + 1}
        </button>
        <CutsList items={this.state.cuts} deleteCallback={this.deleteCut} />
        <br />
        <hr />

        <div>
            <label htmlFor="csvInput" style={{ display: "block" }}>
                Enter CSV File to load Stock or Cuts lists. 
            </label>
            Example stock.csv:
              <table>
                <tbody>
                  <tr><th>size</th><th>cost</th></tr>
                  <tr><td>2700</td><td>21.23</td></tr>
                  <tr><td>3300</td><td>33.65</td></tr>
                </tbody>
              </table>
              Example cuts.csv:
              <table>
                <tbody>
                  <tr><th>size</th><th>count</th></tr>
                  <tr><td>345</td><td>32</td></tr>
                  <tr><td>1000</td><td>6</td></tr>
                </tbody>
              </table>
            
            <input
                onChange={this.handleFileChange}
                id="csvInput"
                name="file"
                type="File"
            />
            <div style={{ marginTop: "3rem" }} className="error">
                {this.state.error}
            </div>
        </div>

        <hr />

        <button onClick={this.getCuttingPlan}
        className={this.state.loading ? 'disabled' : 'enabled'}>
          Get optimum cutting plan
        </button>
        
        {(this.state.apiResult && this.state.apiResult.summary && this.state.apiResult.summary.cost) ? (<button onClick={this.clearPlan}>
          clear
        </button>) : <div></div>}

        
        {(this.state.apiResult && this.state.apiResult.summary && this.state.apiResult.summary.cost) ? (<section>
          <h2>Summary</h2>
          Total Cost: {this.state.apiResult.summary.cost} EUR <br/>
          Total number of items: {this.state.apiResult.summary.pieces}
        </section>) : <section></section> }

        {(this.state.apiResult && this.state.apiResult.order && this.state.apiResult.order.length>0) ? (<section>
          <h2>Order</h2>
          <table>
            <thead>
              <tr><th>Size</th><th>Pieces</th><th>Cost</th></tr>
            </thead>
            <tbody>
              {this.state.apiResult.order.map(item => {
                    item.id = v4(); 
                    return (
                        <tr key={item.id} id={item.id}>
                            <td>{item.size}</td>
                            <td>{item.pieces}</td>  
                            <td>{item.cost.toFixed(2)} EUR</td>  
                        </tr>
                    )
                })}
              </tbody>
          </table>
          <CSVLink 
          filename={"order.csv"}
          className="btn btn-primary"
          target="_blank"
          data={this.state.apiResult.order.map(item=>{
            return {
              size: item.size,
              pieces: item.pieces,
              cost: item.cost
            }
          })}>Download</CSVLink>
        </section>) : <section></section> }

        
        {(this.state.apiResult && this.state.apiResult.cuttingPlan && this.state.apiResult.cuttingPlan.length>0) ? (<section>
          <h2>Cutting plan</h2>
          <table>
            <thead>
              <tr><th>Size</th><th>Count</th><th>Cuts</th></tr>
            </thead>
            <tbody>
              {this.state.apiResult.cuttingPlan.map(item => {
                    item.id = v4(); 
                    return (
                        <tr key={item.id} id={item.id}>
                            <td>{item.size}</td>
                            <td>{item.count}</td>  
                            <td>{item.cuts.join(', ')}</td>
                        </tr>
                    )
                })}
              </tbody>
          </table>
          <CSVLink 
          filename={"cutting-plan.csv"}
          className="btn btn-primary"
          target="_blank"
          data={this.state.apiResult.cuttingPlan.map(item=>{
            return {
              size: item.size,
              count: item.count,
              cuts: item.cuts.join(', ')
            }
          })}>Download</CSVLink>
        </section>) : <section></section> }
      </div>
    );
  }
}

export default Cps;
