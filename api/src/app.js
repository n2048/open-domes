const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const bodyParser = require('body-parser');

require('dotenv').config();

const middlewares = require('./middlewares');
const api = require('./api');

const port = process.env.PORT || 5000;
const url = process.env.URL || 'http://localhost';

const app = express();

app.use(morgan('dev'));
app.use(helmet());
app.use(cors());
app.use(express.json());
// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());
// app.use(bodyParser.raw()); 

app.get('/', (req, res) => {
  const _GET = 'GET';
  const _POST = 'POST';
  res.json({
    endpoints: [
      {
        method: _GET,
        path: `${url}:${port}/api/v1`
      },
      
      {
        method: _GET,
        path: `${url}:${port}/api/v1/csp`        
      },
      {
        method: _POST,
        path: `${url}:${port}/api/v1/csp`        
      },
    ],
  });
});

app.use('/api/v1', api);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
