const express = require('express');

const csp = require('./csp');

const router = express.Router();

router.get('/', (req, res) => {
  res.json({
    message: 'test',
  });
});

router.use('/csp', csp);

module.exports = router;
