const express = require('express');
const howToCutBoards1D = require('stock-cutting').howToCutBoards1D;
const { checkSchema, validationResult } = require('express-validator');

const router = express.Router();

const schema = {
  bladeSize: {
    in: 'body',
    errorMessage: 'bladeSize is undefined',
    isFloat: {
        options:{
          min: 0, 
          max: 10,  
        },
        errorMessage: 'bladeSize must be between 0 and 10 mm',
    },
  },
  stock: {
    in: 'body',
    errorMessage: 'stock is wrong',
    isArray: {
      // bail:true,
      options: {
        min: 0,
      },
    },
  },
  "stock.*.size": {
    isFloat: {
      options:{
        min: 0
      },
      errorMessage: 'stock size must be a positive value',  
    },
  },
  "stock.*.cost": {
    isFloat: {
      options:{
        min: 0
      },
      errorMessage: 'stock cost must be a positive value',  
    },
  },
  cuts: {
    in: 'body',
    errorMessage: 'cuts is wrong',
    isArray: {
      // bail:true,
      options: {
        min: 0,
      },
    },
  },
  "cuts.*.size": {
    isFloat: {
      options:{
        min: 0
      },
      errorMessage: 'cuts size must be a positive value',  
    },
  },
  "cuts.*.count": {
    isInt: {
      options:{
        min: 0
      },
      errorMessage: 'cuts count must be a positive value',  
    },
  },
};

router.post('/', 
checkSchema(schema),
(req, res) => {
  const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

  const data = req.body
  const bladeSize = data.bladeSize || 0;
  const stock = data.stock; 
  const cuts = data.cuts;

  const output = howToCutBoards1D({
    stockSizes: stock,
    bladeSize: bladeSize,
    requiredCuts: cuts,
  });

  const sizes = stock.map(s => s.size);
  const order = sizes.map(s => {
    const d = output.filter(o => o.stock.size === s);
    const pieces = d.map(o => o.count).reduce((a,b) => a + b, 0);
    const cost = d.map(o => o.stock.cost * o.count).reduce((a,b) => a + b, 0);

    return {
      size: s,
      pieces,
      cost
    }
  }).filter(o => o.pieces > 0);

  const summary = {
    cost: order.map(o =>o.cost).reduce((a,b)=>a+b,0), 
    pieces: order.map(o =>o.pieces).reduce((a,b)=>a+b,0)      
  };

  res.json(
    {
      summary,
      order,
      cuttingPlan: output.map(o => {
        return{
          size: o.stock.size,
          count: o.count,
          cuts: o.cuts
        };
      })
    }
  );
});


// for example purposes: 
router.get('/', (req, res) => {
  const bladeSize = 0.125
  const stock = [{ size: 96, cost: 1 }, { size: 24, cost: 0.25 }]
  
  const cuts = [
    { size: 7, count: 21 },
    { size: 76, count: 17 },
    { size: 80, count: 7 },
  ]
  const output = howToCutBoards1D({
    stockSizes: stock,
    bladeSize: bladeSize,
    requiredCuts: cuts,
  })
  res.json(
    {
      stock,
      bladeSize,
      cuts,
      output
    }
  );
});


module.exports = router;
