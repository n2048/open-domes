const app = require('./app');

const port = process.env.PORT || 5000;
const url = process.env.URL || 'http://localhost';

app.listen(port, () => {
  /* eslint-disable no-console */
  console.log(`Listening: ${url}:${port}`);
  /* eslint-enable no-console */
});
