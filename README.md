# open domes


Parametric schematic generation of wood cuts to build a dome based on available material and dome radius.

Use 1-d cutting stock algorithm to minimize waste and lower costs, considering cutting blade width.


## 
A deployed version of this project can be tested here: 
https://neurohub.io/open-dome



## Example docker-compose.yaml 

```
version: "3.9"
services:
  opendome-api:
    container_name: opendome-api
    build: api
    environment:
      - PORT=5555
    ports:
      - 5555:5555
    restart: always
    networks:
      - kong-net

  opendome-client:
    container_name: opendome-client
    build: client
    environment:
      - API_URL=http://localhost:5555
      - PUBLIC_URL=http://localhost:3000
    ports:
      - 3000:3000
    restart: always
```


## Example usage: 

Edit the .env files and run `npm start` in both client and api folders 
 

```
curl --location --request POST 'http://localhost:5555/api/v1/csp' \
--header 'Content-Type: application/json' \
--data-raw '{
    "bladeSize": 3,
    "stock": [
        {"size": 2700,"cost": 16.41},
        {"size": 3000,"cost": 19.20},
        {"size": 3300,"cost": 21.12},
        {"size": 3600,"cost": 21.89},
        {"size": 4200,"cost": 26.90},
        {"size": 4500,"cost": 27.36},
        {"size": 5100,"cost": 32.65}
    ],
    "cuts": [
        {"size": 1267.3,"count": 32},
        {"size": 1389.4,"count": 32},
        {"size": 1568.3,"count": 224},
        {"size": 811.1,"count": 32},
        {"size": 247.7,"count": 32},
        {"size": 1048.6,"count": 32},
        {"size": 1478.7,"count": 32},
        {"size": 1243.2,"count": 32},
        {"size": 540.3, "count": 32}
    ]
}'
```

